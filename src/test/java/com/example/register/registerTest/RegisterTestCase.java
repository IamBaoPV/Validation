package com.example.register.registerTest;

import com.example.register.controller.RegisterController;
import com.example.register.dto.UserDto;
import com.example.register.model.User;
import com.example.register.repository.UserRepository;
import com.example.register.service.RegisterService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(RegisterController.class)
public class RegisterTestCase {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private RegisterService registerService;
    private UserDto userDto ;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MvcResult mvcResult;

    @BeforeEach
    public void setUp(){
        //Init MockMvc Object and build
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        userDto = new UserDto();
        userDto.setUsername("IamBaopv");
        userDto.setPassword("Baopv123@");
        userDto.setConfirmPassword("Baopv123@");
        userDto.setEmail("baopv1234@gmail.com");
        userDto.setPhoneNumber("0123456789");
        userDto.setPermission("Merchant admin");
    }
    public MvcResult postRequest(UserDto dto) throws Exception {
        String userJson = new Gson().toJson(dto);
        MvcResult result =  mockMvc.perform(MockMvcRequestBuilders.post("/api/create")
                        .characterEncoding("UTF-8")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(userJson))
                .andDo(print())
                .andReturn();
        return result;
    }
    //FUNCTIONAL
    @Test
    public void givenUserSuccess_whenEnterAllCase_thenStatus201() throws Exception {
        userDto.setPhoneNumber("");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.CREATED.value(), statusCode);
    }

    @Test
    public void givenUserSuccess_whenEnterRequiredCase_thenStatus201() throws Exception {
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.CREATED.value(), statusCode);
    }

    @Test
    public void givenUser_whenAllCaseIsEmpty_thenError() throws Exception {
        userDto.setUsername("");
        userDto.setPassword("");
        userDto.setConfirmPassword("");
        userDto.setEmail("");
        userDto.setPhoneNumber("");
        List<String> expectedData = List.of("Tên đăng nhập không được để trống",
                "Mật khẩu không được để trống",
                "Nhập lại mật khẩu không được để trống",
                "Email không được để trống");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.BAD_REQUEST.value(), statusCode);

        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.size(), responseList.size());
    }

    @Test
    public void givenUser_whenRequiredCaseIsEmpty_thenError() throws Exception {
        userDto.setUsername("");
        userDto.setPassword("");
        userDto.setConfirmPassword("");
        userDto.setEmail("");
        List<String> expectedData = List.of("Tên đăng nhập không được để trống",
                "Mật khẩu không được để trống",
                "Nhập lại mật khẩu không được để trống",
                "Email không được để trống");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.BAD_REQUEST.value(), statusCode);

        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {});
        assertEquals(expectedData.size(), responseList.size());
    }

    // VALIDATE TÊN ĐĂNG NHẬP
    @Test
    public void givenUser_whenUsernameIsEmpty_thenError() throws Exception {
        userDto.setUsername("");
        List<String> expectedData = List.of("Tên đăng nhập không được để trống");
         mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.BAD_REQUEST.value(), statusCode);

        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenLengthUsernameEqualMinlengthIs1_whenAllCaseIsTrue_thenStatus201() throws Exception {
        userDto.setUsername("H");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.CREATED.value(), statusCode);
    }

    @Test
    public void givenLengthUsernameEqualMaxlengthIs200_whenAllCaseIsTrue_thenStatus201() throws Exception {
        userDto.setUsername("LhyXQGVB392WtUmhGgCqqtXB2LzYqWTXpfm9NhTKNcXR3jZ9DgaH9SpnP96AgirqG" +
                "VgHBmyCVjf248DMfnbX8dQcGqfHzxu8p4CSy3pXqXyw5t2x4Sj6Qz3a43LTVqHnAEhWv6CXg6T8GQW" +
                "rmyqiAXq8qhSbCuF6pcvUZF9xb6eBGUcjkgdtB7rDRTrgAtTQr3br8iFV");
         mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.CREATED.value(), statusCode);
    }

    @Test
    public void givenLengthUsernameEqualOverMaxlengthIs201_whenAllCaseIsTrue_thenError() throws Exception {
        userDto.setUsername("LhyXQGVB392WtUmhGgCqqtXB2LzYqWTXpfm9NhTKNcXR3jZ9DgaH9SpnP96AgirqG" +
                "VgHBmyCVjf248DMfnbX8dQcGqfHzxu8p4CSy3pXqXyw5t2x4Sj6Qz3a43LTVqHnAEhWv6CXg6T8GQW" +
                "rmyqiAXq8qhSbCuF6pcvUZF9xb6eBGUcjkgdtB7rDRTrgAtTQr3br8iFVa");
        List<String> expectedData = List.of("Tên đăng nhập chỉ tối đa 200 ký tự");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(400, statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void giveUsernameContainSpaceStartAndEnd_whenAllCaseIsTrue_thenStatus201() throws Exception {
        userDto.setUsername(" PhamVanBao ");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.CREATED.value(), statusCode);

        String content = mvcResult.getResponse().getContentAsString();
        System.out.println(content);
    }

    @Test
    public void givenUsernameContainFullSpace_whenAllCaseIsTrue_thenError() throws Exception {
        userDto.setUsername("      ");
        List<String> expectedData = List.of("Tên đăng nhập không được để trống");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(400, statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenUsernameContainSpecialCharacter_whenAllCaseIsTrue_thenError() throws Exception {
        userDto.setUsername("PhamVanBao++");
        mvcResult = postRequest(userDto);
        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(201, statusCode);
    }

    @Test
    public void givenUsernameContainFullSpecialCharacter_whenAllCaseIsTrue_thenError() throws Exception {
        userDto.setUsername("!@#$%^&*++");
        mvcResult = postRequest(userDto);
        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(201, statusCode);
    }

    @Test
    public void givenUsernameContainFullLowerCase_whenAllCaseIsTrue_thenError() throws Exception {
        userDto.setUsername("testcase");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(201, statusCode);
    }

    @Test
    public void givenUsernameContainFullUpperCase_whenAllCaseIsTrue_thenError() throws Exception {
        userDto.setUsername("TESTCASE");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(201, statusCode);
    }

    @Test
    public void givenUsernameContainFullNumber_whenAllCaseIsTrue_thenError() throws Exception {
        userDto.setUsername("12345678");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(201, statusCode);
    }
    @Test
    public void givenUsernameContainFullSize_whenAllCaseIsTrue_thenError() throws Exception {
        userDto.setUsername("abc２３");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(201, statusCode);
    }

    @Test
    public void givenUsernameIsExist_whenAllCaseIsTrue_thenError() throws Exception {
        userDto.setUsername("Baopv");
        userDto.setPassword("Phamvanbao123.");
        userDto.setConfirmPassword("Phamvanbao123.");

        // Create 1 account with username is exist
        User user = new User(1L,"Baopv","PhamBao1234@","phamvanbao297@gmail.com","","Merchant admin");

        when(userRepository.findUserByUsername(user.getUsername())).thenReturn(user);

        List<String> expectedData = List.of("Tên đăng nhập đã tồn tại trong hệ thống.");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(400, statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    // VALIDATE MẬT KHẨU
    @Test
    public void givenPassword_whenPasswordIsEmpty_thenError() throws Exception {
        userDto.setPassword("");
        List<String> expectedData = List.of("Mật khẩu không được để trống");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.BAD_REQUEST.value(), statusCode);

        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenPasswordHaveLength_whenPasswordIsMinlengthIs7AndTrueFormat_thenStatus201() throws Exception {
        userDto.setPassword("Abc123@");
        userDto.setConfirmPassword("Abc123@");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.CREATED.value(), statusCode);
    }

    @Test
    public void givenPasswordHaveLength_whenPasswordIsMinlengthIs7AndFalseFormat_thenError() throws Exception {
        userDto.setPassword("Abc1234");
        userDto.setConfirmPassword("Abc1234");
        List<String> expectedData = List.of("Mật khẩu mới  phải có 7 ký tự trở lên và dưới 50 ký tự." +
                " Cấu tạo bao gồm ký tự số, chữ thường, chữ hoa, các ký tự đặc biệt");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.BAD_REQUEST.value(), statusCode);

        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenPasswordHaveLength_whenPasswordIsLessMinlengthIs6AndTrueFormat_thenError() throws Exception {
        userDto.setPassword("Abc12@");
        userDto.setConfirmPassword("Abc12@");
        List<String> expectedData = List.of("Mật khẩu mới  phải có 7 ký tự trở lên và dưới 50 ký tự." +
                " Cấu tạo bao gồm ký tự số, chữ thường, chữ hoa, các ký tự đặc biệt");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.BAD_REQUEST.value(), statusCode);

        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenPasswordHaveLength_whenPasswordIsMaxlengthIs50AndTrueFormat_thenStatus201() throws Exception {
        userDto.setPassword("zKhfWu4EtYADb3us7EfsezM6j7zLAAKMLusrz0J1fDnWG7///@");
        userDto.setConfirmPassword("zKhfWu4EtYADb3us7EfsezM6j7zLAAKMLusrz0J1fDnWG7///@");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.CREATED.value(), statusCode);
    }

    @Test
    public void givenPasswordHaveLength_whenPasswordIsMaxlengthIs50AndFalseFormat_thenError() throws Exception {
        userDto.setPassword("zKhfWu4EtYADb3us7EfsezM6j7zLAAKMLusrz0J1fDnWG7lmUQ");
        userDto.setConfirmPassword("zKhfWu4EtYADb3us7EfsezM6j7zLAAKMLusrz0J1fDnWG7lmUQ");
        List<String> expectedData = List.of("Mật khẩu mới  phải có 7 ký tự trở lên và dưới 50 ký tự." +
                " Cấu tạo bao gồm ký tự số, chữ thường, chữ hoa, các ký tự đặc biệt");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.BAD_REQUEST.value(), statusCode);

        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenPasswordHaveLength_whenPasswordIsOverMaxlengthIs51AndTrueFormat_thenError() throws Exception {
        userDto.setPassword("zKhfWu4EtYADb3us7EfsezM6j7zLAAKMLusrz0J1fDnWG7lmUQ@");
        userDto.setConfirmPassword("zKhfWu4EtYADb3us7EfsezM6j7zLAAKMLusrz0J1fDnWG7lmUQ@");
        List<String> expectedData = List.of("Mật khẩu mới  phải có 7 ký tự trở lên và dưới 50 ký tự." +
                " Cấu tạo bao gồm ký tự số, chữ thường, chữ hoa, các ký tự đặc biệt");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.BAD_REQUEST.value(), statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenPasswordHaveLength_whenPasswordIsMidOfMinlengthAndMaxlengthAndTrueFormat_thenStatus201() throws Exception {
        userDto.setPassword("zKhfWu4EtYADb3us7EfsezM6j7z@@");
        userDto.setConfirmPassword("zKhfWu4EtYADb3us7EfsezM6j7z@@");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.CREATED.value(), statusCode);
    }

    @Test
    public void givenPasswordContainUsername_whenPasswordIsTrueFormat_thenError() throws Exception {
        userDto.setUsername("Baopv");
        List<String> expectedData = List.of("Bạn vui lòng thiết lập mật khẩu khác với Tên đăng nhập");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.BAD_REQUEST.value(), statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenPasswordContainSpace_whenPasswordIsTrueFormat_thenError() throws Exception {
        userDto.setPassword(" abc123D@ ");
        userDto.setConfirmPassword(" abc123D@ ");
        List<String> expectedData = List.of("Mật khẩu mới  phải có 7 ký tự trở lên và dưới 50 ký tự." +
                " Cấu tạo bao gồm ký tự số, chữ thường, chữ hoa, các ký tự đặc biệt");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.BAD_REQUEST.value(), statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenPassword_whenPasswordContainNoNumber_thenError() throws Exception {
        userDto.setPassword("Baopham@");
        userDto.setConfirmPassword("Baopham@");
        List<String> expectedData = List.of("Mật khẩu mới  phải có 7 ký tự trở lên và dưới 50 ký tự." +
                " Cấu tạo bao gồm ký tự số, chữ thường, chữ hoa, các ký tự đặc biệt");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.BAD_REQUEST.value(), statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenPassword_whenPasswordContainNoLowerCase_thenError() throws Exception {
        userDto.setPassword("BAOPV123@");
        userDto.setConfirmPassword("BAOPV123@");
        List<String> expectedData = List.of("Mật khẩu mới  phải có 7 ký tự trở lên và dưới 50 ký tự." +
                " Cấu tạo bao gồm ký tự số, chữ thường, chữ hoa, các ký tự đặc biệt");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.BAD_REQUEST.value(), statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenPassword_whenPasswordContainNoUpperCase_thenError() throws Exception {
        userDto.setPassword("baopv123@");
        userDto.setConfirmPassword("baopv123@");
        List<String> expectedData = List.of("Mật khẩu mới  phải có 7 ký tự trở lên và dưới 50 ký tự." +
                " Cấu tạo bao gồm ký tự số, chữ thường, chữ hoa, các ký tự đặc biệt");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.BAD_REQUEST.value(), statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenPassword_whenPasswordContainNoSpecialCharacter_thenError() throws Exception {
        userDto.setPassword("Baopv1234");
        userDto.setConfirmPassword("Baopv1234");
        List<String> expectedData = List.of("Mật khẩu mới  phải có 7 ký tự trở lên và dưới 50 ký tự." +
                " Cấu tạo bao gồm ký tự số, chữ thường, chữ hoa, các ký tự đặc biệt");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.BAD_REQUEST.value(), statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenPassword_whenPasswordContainFullSizeCharacter_thenError() throws Exception {
        userDto.setPassword("Abcd２３@");
        userDto.setConfirmPassword("Abcd２３@");
        List<String> expectedData = List.of("Mật khẩu mới  phải có 7 ký tự trở lên và dưới 50 ký tự." +
                " Cấu tạo bao gồm ký tự số, chữ thường, chữ hoa, các ký tự đặc biệt");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.BAD_REQUEST.value(), statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    // VALIDATE NHẬP LẠI MẬT KHẨU
    @Test
    public void givenConfirmPassword_whenConfirmPasswordIsEmpty_thenError() throws Exception {
        userDto.setConfirmPassword("");
        List<String> expectedData = List.of("Nhập lại mật khẩu không được để trống");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.BAD_REQUEST.value(), statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenConfirmPassword_whenConfirmPasswordNotMatch_thenError() throws Exception {
        userDto.setConfirmPassword("abc123");
        List<String> expectedData = List.of("Nhập lại mật khẩu không trùng với mật khẩu");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.BAD_REQUEST.value(), statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
        assertEquals(expectedData.size(), responseList.size());
    }

    @Test
    public void givenConfirmPasswordContainFullsizeCharacter_whenConfirmPasswordMatch_thenError() throws Exception {
        userDto.setPassword("Baopv23@");
        userDto.setConfirmPassword("Baopv２３@");
        List<String> expectedData = List.of("Nhập lại mật khẩu không trùng với mật khẩu");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.BAD_REQUEST.value(), statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
        assertEquals(expectedData.size(), responseList.size());
    }

    @Test
    public void givenConfirmPassword_whenConfirmPasswordMatch_thenStatus201() throws Exception {
        userDto.setConfirmPassword("Baopv123@");
        mvcResult = postRequest(userDto);
        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(201,statusCode);
    }

    @Test
    public void givenConfirmPasswordContainSpace_whenConfirmPasswordMatch_thenStatus201() throws Exception {
        userDto.setConfirmPassword(" Baopv123@ ");
        mvcResult = postRequest(userDto);
        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(201,statusCode);
    }

    @Test
    public void givenConfirmPasswordContainSpaceMiddle_whenConfirmPasswordMatch_thenError() throws Exception {
        userDto.setConfirmPassword("Baopv 123@");
        List<String> expectedData = List.of("Nhập lại mật khẩu không trùng với mật khẩu");
        mvcResult = postRequest(userDto);

        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.BAD_REQUEST.value(), statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
        assertEquals(expectedData.size(), responseList.size());
    }

    //VALIDATE EMAIL
    @Test
    public void givenEmail_whenEmailIsCorrectFormat_thenStatus201() throws Exception {
        userDto.setEmail("Baopv123@gmail.com");
        mvcResult = postRequest(userDto);
        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(201,statusCode);
    }

    @Test
    public void givenEmail_whenEmailIsEmpty_thenError() throws Exception {
        userDto.setEmail("");
        List<String> expectedData = List.of("Email không được để trống");
        mvcResult = postRequest(userDto);
        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(400,statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenEmail_whenEmailIsFullSpace_thenError() throws Exception {
        userDto.setEmail("       ");
        List<String> expectedData = List.of("Email không được để trống");
        mvcResult = postRequest(userDto);
        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(400,statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenEmailHaveLength_whenEmailIsMaxlength200AndTrueFormat_thenStatus201() throws Exception {
        userDto.setEmail("ubv7oe9NnBu6xIdO0W4alzy.GBMom9D5FaXB8LGAplSzLaHUyydVO0gHWv.CNhSc6" +
                "M6HcGQ5Aenajui9G5gfZQ4pCL8upMl482ZtkJI.p43Dm.oqiUHZU0D8gHwI6KBr0jKKtjK7ETwI" +
                "TCd7oEb3xqD7Y0hcd8TxVzf7wcuYsW0OmWMMUGmaFGQnQKW688@gmail.com");
        mvcResult = postRequest(userDto);
        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(201,statusCode);
    }

    @Test
    public void givenEmailHaveLength_whenEmailIsMaxlength200AndFalseFormat_thenError() throws Exception {
        userDto.setEmail("ubv7oe9NnBu6xIdO0W4alzy.GBMom9D5FaXB8LGAplSzLaHUyydVO0gHWv.CNhSc6" +
                "M6HcGQ5Aenajui9G5gfZQ4pCL8upMl482ZtkJI.p43Dm.oqiUHZU0D8gHwI6KBr0jKKtjK7ETwI" +
                "TCd7oEb3xqD7Y0hcd8TxVzf7wcuYsW0OmWMMUGmaFGQnQKW688.gmail.com");
        List<String> expectedData = List.of("Email sai định dạng");
        mvcResult = postRequest(userDto);
        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(400,statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenEmailHaveLength_whenEmailIsOverMaxlength201AndFalseFormat_thenError() throws Exception {
        userDto.setEmail("ubv7oe9NnBu6xIdO0W4alzy.GBMom9D5FaXB8LGAplSzLaHUyydVO0gHWv.CNhSc6" +
                "M6HcGQ5Aenajui9G5gfZQ4pCL8upMl482ZtkJI.p43Dm.oqiUHZU0D8gHwI6KBr0jKKtjK7ETwI" +
                "TCd7oEb3xqD7Y0hcd8TxVzf7wcuYsW0OmWMMUGmaFGQnQKW6888@gmail.com");
        List<String> expectedData = List.of("Email sai định dạng");
        mvcResult = postRequest(userDto);
        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(400,statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenEmailHaveLength_whenEmailIsMidMinlengthAndMaxlengthAndTrueFormat_thenStatus201() throws Exception {
        userDto.setEmail("ubv7oe9NnBu6xIdO0W4alzy.GBMom9D5FaXB8LGAplSzLaHUyydVO0gHWv.CNhSc6M6HcGQ5Aenajui9G5gfZQ4pCL@gmail.com");
        mvcResult = postRequest(userDto);
        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(201,statusCode);
    }

    @Test
    public void givenEmail_whenEmailContainFullNumber_thenError() throws Exception {
        userDto.setEmail("123456789");
        List<String> expectedData = List.of("Email sai định dạng");
        mvcResult = postRequest(userDto);
        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(400,statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenEmail_whenEmailNoLocalPart_thenError() throws Exception {
        userDto.setEmail("@gmail.com");
        List<String> expectedData = List.of("Email sai định dạng");
        mvcResult = postRequest(userDto);
        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(400,statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenEmail_whenEmailNoDomainName_thenError() throws Exception {
        userDto.setEmail("baopv123@");
        List<String> expectedData = List.of("Email sai định dạng");
        mvcResult = postRequest(userDto);
        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(400,statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenEmail_whenEmailNoConnectionPart_thenError() throws Exception {
        userDto.setEmail("baopv123gmail.com");
        List<String> expectedData = List.of("Email sai định dạng");
        mvcResult = postRequest(userDto);
        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(400,statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenEmail_whenEmailHaveManyConnectionPart_thenError() throws Exception {
        userDto.setEmail("baopv123@gmail.comabc@runsystem.net");
        List<String> expectedData = List.of("Email sai định dạng");
        mvcResult = postRequest(userDto);
        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(400,statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenEmail_whenEmailContainSpaceMidCharacter_thenError() throws Exception {
        userDto.setEmail("bao p v123@gmail.com");
        List<String> expectedData = List.of("Email sai định dạng");
        mvcResult = postRequest(userDto);
        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(400,statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenEmail_whenEmailContainCharacter_thenError() throws Exception {
        userDto.setEmail("bao/pv123@gmail.com");
        List<String> expectedData = List.of("Email sai định dạng");
        mvcResult = postRequest(userDto);
        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(400,statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenEmail_whenEmailContainDotBeforeConnection_thenError() throws Exception {
        userDto.setEmail("baopv123.@gmail.com");
        List<String> expectedData = List.of("Email sai định dạng");
        mvcResult = postRequest(userDto);
        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(400,statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenEmail_whenEmailContainDotAfterConnection_thenError() throws Exception {
        userDto.setEmail("baopv123@.gmail.com");
        List<String> expectedData = List.of("Email sai định dạng");
        mvcResult = postRequest(userDto);
        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(400,statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenEmail_whenEmailContain2ConsecutiveDots_thenError() throws Exception {
        userDto.setEmail("baopv..123@gmail.com");
        List<String> expectedData = List.of("Email sai định dạng");
        mvcResult = postRequest(userDto);
        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(400,statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenEmail_whenEmailContainDotsStartEnd_thenError() throws Exception {
        userDto.setEmail(".baopv123@gmail.com.");
        List<String> expectedData = List.of("Email sai định dạng");
        mvcResult = postRequest(userDto);
        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(400,statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenEmail_whenEmailContainSpaceBeforeConnection_thenError() throws Exception {
        userDto.setEmail("baopv123 @gmail.com");
        List<String> expectedData = List.of("Email sai định dạng");
        mvcResult = postRequest(userDto);
        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(400,statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenEmail_whenEmailContainSpaceAfterConnection_thenError() throws Exception {
        userDto.setEmail("baopv123@ gmail.com");
        List<String> expectedData = List.of("Email sai định dạng");
        mvcResult = postRequest(userDto);
        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(400,statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenEmail_whenEmailContainFullSizeCharacter_thenError() throws Exception {
        userDto.setEmail("baopv２３@gmail.com");
        List<String> expectedData = List.of("Email sai định dạng");
        mvcResult = postRequest(userDto);
        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(400,statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    @Test
    public void givenEmail_whenEmailIsExist_thenError() throws Exception {
        userDto.setEmail("baopv123@gmail.com");
        User user = new User(1L,"Baopv","Baopham123@","baopv123@gmail.com","","Merchant admin");
        when(userRepository.findUserByEmail(user.getEmail())).thenReturn(user);
        List<String> expectedData = List.of("Email đã tồn tại trong hệ thống.");
        mvcResult = postRequest(userDto);
        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(400,statusCode);
        String responseContent = new String(mvcResult.getResponse().getContentAsByteArray(), StandardCharsets.UTF_8);
        List<String> responseList = new ObjectMapper().readValue(responseContent, new TypeReference<>() {
        });
        assertEquals(expectedData.get(0), responseList.get(0));
    }

    //VALIDATE PHONE NUMBER
    @Test
    public void givenPhoneNumber_whenPhoneNumberIsEmpty_thenStatus201() throws Exception {
        userDto.setPhoneNumber("");
        mvcResult = postRequest(userDto);
        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(201,statusCode);
    }

    @Test
    public void givenPhoneNumber_whenPhoneNumberContainFullSpace_thenStatus201() throws Exception {
        userDto.setPhoneNumber("      ");
        mvcResult = postRequest(userDto);
        int statusCode = mvcResult.getResponse().getStatus();
        assertEquals(201,statusCode);
    }

}
