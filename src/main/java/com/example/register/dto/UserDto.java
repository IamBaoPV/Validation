package com.example.register.dto;

import com.example.register.custom.ValidEmail;
import com.example.register.custom.ValidPassword;
import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor(staticName = "build")
@NoArgsConstructor
public class UserDto {
    @NotBlank(message = "Tên đăng nhập không được để trống")
    @Size(max = 200, message = "Tên đăng nhập chỉ tối đa 200 ký tự")
    private String username;

    @ValidPassword(notEmpty = true)
    private String password;

    @NotBlank(message = "Nhập lại mật khẩu không được để trống")
    private String confirmPassword;

    @ValidEmail(notEmpty = true)
    private String email;

    private String phoneNumber;

    private String permission;
}
