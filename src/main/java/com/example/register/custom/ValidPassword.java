package com.example.register.custom;

import jakarta.validation.Constraint;

import java.lang.annotation.*;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PasswordValidator.class)
@Documented
public @interface ValidPassword {
    String message() default "Mật khẩu sai định dạng";
    String messageNotEmpty() default "Mật khẩu không được để trống";

    String messageFormatPassword() default "Mật khẩu mới  phải có 7 ký tự trở lên và dưới 50 ký tự. " +
            "Cấu tạo bao gồm ký tự số, chữ thường, chữ hoa, các ký tự đặc biệt";
    boolean notEmpty() default false;


    Class<?>[] groups() default {};

    Class<?>[] payload() default {};
}
