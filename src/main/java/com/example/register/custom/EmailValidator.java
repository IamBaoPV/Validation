package com.example.register.custom;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.regex.Pattern;

public class EmailValidator implements ConstraintValidator<ValidEmail,String> {
    private Boolean notEmpty;
    private String messageNotEmpty;
    private String messageFormatEmail;

    private static final Pattern PASSWORD_PATTERN = Pattern.compile("^(?=.{1,200}$)[a-zA-Z0-9]+(?:\\.[a-zA-Z0-9]+)*@[a-zA-Z0-9]+(?:\\.[a-zA-Z0-9]+)*$");

    @Override
    public void initialize(ValidEmail field) {
        notEmpty = field.notEmpty();
        messageNotEmpty = field.messageNotEmpty();
        messageFormatEmail = field.messageFormatEmail();
    }

    @Override
    public boolean isValid(String email, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        if (notEmpty && email.trim().isEmpty()) {
            context.buildConstraintViolationWithTemplate(messageNotEmpty).addConstraintViolation();
            return false;
        }
        if(!PASSWORD_PATTERN.matcher(email).matches()){
            context.buildConstraintViolationWithTemplate(messageFormatEmail).addConstraintViolation();
            return false;
        }
        return true;
    }
}
