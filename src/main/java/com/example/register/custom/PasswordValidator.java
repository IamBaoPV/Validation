package com.example.register.custom;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.regex.Pattern;

public class PasswordValidator implements ConstraintValidator<ValidPassword, String> {
    private Boolean notEmpty;
    private String messageNotEmpty;
    private String messageFormatPassword;
    private static final Pattern PASSWORD_PATTERN = Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=~!()*_\\\\\\-\\/+:;\"'.,<>`])(?=\\S+$).{7,50}$");

    @Override
    public void initialize(ValidPassword field) {
//        ConstraintValidator.super.initialize(constraintAnnotation);
        notEmpty = field.notEmpty();
        messageNotEmpty = field.messageNotEmpty();
        messageFormatPassword = field.messageFormatPassword();
    }

    @Override
    public boolean isValid(String password, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        if (notEmpty && password.trim().isEmpty()) {
            context.buildConstraintViolationWithTemplate(messageNotEmpty).addConstraintViolation();
            return false;
        }
        if(!PASSWORD_PATTERN.matcher(password).matches()){
            context.buildConstraintViolationWithTemplate(messageFormatPassword).addConstraintViolation();
            return false;
        }
        return true;
    }

}
