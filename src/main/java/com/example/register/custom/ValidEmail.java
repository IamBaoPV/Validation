package com.example.register.custom;

import jakarta.validation.Constraint;

import java.lang.annotation.*;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = EmailValidator.class)
@Documented
public @interface ValidEmail {
    String message() default "Email sai định dạng";
    String messageNotEmpty() default "Email không được để trống";

    String messageFormatEmail() default "Email sai định dạng";
    boolean notEmpty() default false;


    Class<?>[] groups() default {};

    Class<?>[] payload() default {};
}
