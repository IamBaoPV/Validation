package com.example.register.controller;

import com.example.register.dto.UserDto;
import com.example.register.model.User;
import com.example.register.repository.UserRepository;
import com.example.register.service.RegisterService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class RegisterController {
    static Logger logger = Logger.getLogger(RegisterController.class.getName());
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RegisterService registerService;

    @PostMapping("/create")
    public ResponseEntity<?> registerUser(@RequestBody @Valid UserDto userDto, BindingResult result) {
        FieldError error;

        if(!userDto.getConfirmPassword().trim().equals("") && !userDto.getPassword().trim().equals("")){
            if (!(userDto.getConfirmPassword().trim().equals(userDto.getPassword().trim()))) {
                error = new FieldError("password", "password", "Nhập lại mật khẩu không trùng với mật khẩu");
                result.addError(error);
                logger.warning(error.getDefaultMessage());
            }
        }
        if(!userDto.getUsername().trim().equals("")) {
            if (userRepository.findUserByUsername(userDto.getUsername().trim()) != null) {
                error = new FieldError("username", "username", "Tên đăng nhập đã tồn tại trong hệ thống.");
                result.addError(error);
                logger.warning(error.getDefaultMessage());
            }
        }

        if(!userDto.getUsername().trim().equals("")) {
            if (userDto.getPassword().trim().contains(userDto.getUsername().trim())) {
                error = new FieldError("contain", "contain", "Bạn vui lòng thiết lập mật khẩu khác với Tên đăng nhập");
                result.addError(error);
                logger.warning(error.getDefaultMessage());
            }
        }

        if(!userDto.getEmail().trim().equals("")) {
            if (userRepository.findUserByEmail(userDto.getEmail().trim()) != null) {
                error = new FieldError("email", "email", "Email đã tồn tại trong hệ thống.");
                result.addError(error);
                logger.warning(error.getDefaultMessage());
            }
        }

        if(result.hasErrors()){
            List<String> errors = result.getFieldErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.toList());
            return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
        }else {
            User user = registerService.register(userDto);
            logger.info("Đăng ký thành công");
            return new ResponseEntity<>(user,HttpStatus.CREATED);
        }
    }
}
