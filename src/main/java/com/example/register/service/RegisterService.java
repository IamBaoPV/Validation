package com.example.register.service;

import com.example.register.dto.UserDto;
import com.example.register.model.User;
import com.example.register.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class RegisterService {
    @Autowired
    private UserRepository userRepository;

    public User register(UserDto userDto){
        User user = new User();
        user.setUsername(userDto.getUsername().trim());
        user.setPassword(userDto.getPassword());
        user.setEmail(userDto.getEmail().trim());
        user.setPhoneNumber(userDto.getPhoneNumber().trim());
        user.setPermission(userDto.getPermission());
        return userRepository.save(user);
    }
}
