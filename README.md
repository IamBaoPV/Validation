# Đăng ký người dùng

###### 1.Nội dung: 

- Cho phép người dùng sử dụng quyền System admin / Merchant admin thực hiện đăng ký người dùng

###### 2.Yêu cầu chung:

- Tất cả các màn hình đăng ký, cập nhật nếu đã thay đổi ít nhất một trường trong màn hình khi chuyển trang phải hiển thị popup xác nhận trước khi chuyển trang. Popup gồm thông báo “Tất cả dữ liệu bạn thay đổi không được lưu. /n Bạn có chắc chắn muốn rời khỏi trang này?” và button OK, button X.
    - Ấn vào button OK=> đóng popup và di chuyển đến trang tương ứng
    - Ấn vào button X thì đóng popup, cho phép sửa các trường tại màn hình hiện tại, giá trị các trường đã nhập được giữ nguyên.
- Tất cả các màn hình đăng ký không thay đổi tất cả các trường trong màn hình thì ấn vào button back di chuyển đến màn hình Danh sách người dùng.
- Tất cả các màn hình đăng ký: Khi ấn nút đăng ký: nếu thành công thì hiển thị popup thông báo thành công, nếu thất bại thì hiển thị thông báo ngay tại màn hình đó. 
- Khi open màn hình, con trỏ chuột sẽ mặc định focus ở vị trí input đầu tiên
- Định dang hiển thị con trỏ chuột : Trong trường hợp hover qua text read only  hiển thị hình mũi tên, Hover qua checkbox, radio button, button, link hiển thị hình bàn tay

###### 3. Mô tả chứ năng:

- Chức năng cho phép người sử dụng có quyền System admin thực hiện đăng ký người dùng mới cho hệ thống.

###### 4. Yêu cầu chức năng:

- System admin có thể đăng ký  những user như : Merchant admin, Merchant admin ABC, Transaction admin, Approver.
- Merchant admin không có quyền tạo Merchant ở chức năng quản lý user, để tạo Merchant thì Merchant admin vào chức năng quản lý merchant
- Tên đăng nhập không được trùng.
- Người dùng mới đăng ký mặc định ở trạng thái active.
- Email không được trùng.

###### 5. Các mục màn hình
|STT|Hạng mục/Tên trường|Kiểu dữ liệu|Control|Bắt buộc|Mô tả|
|:---:|:---|:---|:---:|:---:|:---|
|1|Tên đăng nhập|Varchar200|Textbox|Có|- Tên đăng nhập không được trùng<br>- Cho phép nhập số, chữ thường, chữ hoa, khoảng trắng và ký tự đặc biệt<br>- Placeholder là "Nhập tên đăng nhập"|
|2|Mật khẩu|Varchar50|Textbox|Có|- Hiển thị dưới dạng ký tự "."<br>- Check mật khẩu phải có ít nhất 7 ký tự trở lên gồm 4 loại ký tự:Ký tự số, chữ thường, chữ hoa,các ký tự đặc biệt<br>- Placeholder là "********"|
|3|Nhập lại mật khẩu|Varchar50|Textbox|Có|- Validate trùng với mật khẩu<br>- Hiển thị dưới dạng ký tự "."|
|4|Email|Varchar200|Textbox|Có|- Validate email theo định dạng.<br>+  Email có dạng Local-part@DomainName<br>+  Trong tên DomainName có dấu chấm không có ký tự đặc biệt, sau dấu chấm phải có ký tự<br>+ Trước và sau @ không có dấu cách<br>- Kiểm tra Email đã tồn tại trong hệ thống chưa<br>- Placeholder là "Nhập email"|
|5|Số điện thoại|NuABCer 13|Textbox|-|- Chỉ nhận nhập ký tự số, tối đa 13 ký tự<br>- Placeholder là "Nhập số điện thoại"|
|6|Quyền|-|DropdownList|-|- Hiện tại có 6 quyền đó là: System admin, Appover, Merchant admin, Transaction admin, Merchant, Merchant admin ABC<br>- Mặc định ban đầu của quyền là Merchant admin. Quyền Merchant, System admin không hiện trong DropdownList"|
|7|Đăng ký|-|Button|-|- Validate các trường thỏa mãn và server trả về thành công thì thêm user vào CSDL, hiển thị popup thông báo thành công<br>- Validate các trường không thành công thì hiển thị thông báo lỗi dưới các trường đó|
|8|Popup thông báo|-|Label|-|Hiển thị thông báo "Đăng ký thành công"|
|9|OK(popup)|-|Button|-|Ấn vào button thì đóng popup và di chuyển đến màn hình danh sách user|
|10|Back|-|Button|-|- Ấn vào button Back nếu đã thay đổi ít nhất 1 trường trên màn hình thì hiển thị popup thông báo "Tất cả dữ liệu bạn thay đổi sẽ không được lưu/n Bạn có chắc chắn muốn rời khỏi trang này?, button OK và button Close<br>+ Ấn vào button OK thì di chuyển đến màn hình danh sách user và không lưu dữ liệu vào DB<br>+ Ấn vào button Close đóng popup và cho phép tiếp tục thay đổi các trường màn hình<br>- Ấn vào button Back nếu không thay đổi tất cả các trường trên màn hình thì quay về màn hình trước đó là màn hình danh sách user|
|11|Thông báo lỗi|-|Label|-|Hiển thị thông báo lỗi|

###### 6. Lỗi
|STT|Mô tả lỗi|Mô tả xử lý lỗi|
|:---:|:---|:---|
|1|Bỏ trống 1 trong các trường: Mật khẩu, Tên đăng nhập, Email| Hiển thị thông báo lỗi:<br>"[Tên trường] không được để trống"|
|2|Nhập lại mật khẩu không trùng khớp với mật khẩu| Hiển thị thông báo lỗi:<br>"Nhập lại mật khẩu không trùng với mật khẩu"|
|3|Mật khẩu không thỏa mãn 1 trong các điều kiện:<br>- Có 7 ký tự trở lên.<br>- Cấu tạo gồm: ký tự số, chữ thường, chữ hoa, các ký tự đặc biệt| Hiển thị thông báo lỗi:<br>"Mật khẩu mới phải có 7 ký tự trở lên. Cấu tạo bao gồm ký tự số, chữ thường, chữ hoa, các ký tự đặc biệt"|
|4|Email không đúng định dạng| Hiển thị thông báo lỗi:<br>"Email không đúng định dạng"|
|5|một trong các trường: Email, Tên đăng nhập đã tồn tại trong hệ thống| Hiển thị thông báo lỗi:<br>"[Tên trường] đã tồn tại trong hệ thống"|
